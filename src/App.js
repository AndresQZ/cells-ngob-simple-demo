import React, { Component } from 'react';
import './App.css';
import Post from './Post/component/Post';
import PostEditor from './PostEditor/component/PostEditor';


class App extends Component {
  constructor(props) {
     super(props);
	 
       // hack para asegurar que las funciones tiene el contexto del componente
      this.addPost = this.addPost.bind(this);
	 
	 
      this.state = {
       posts :[]
      }   
  }
	
	    addPost (newPostBody) {
			
			// copiar el estado
            const newState = Object.assign({}, this.state);
            // incluir el nuevo mensaje
            newState.posts.push(newPostBody);
            // actualizar el estado
            this.setState(newState);

        }

         
	
	
  render() {
    return (
      <div>
	  
	    <div className="panel panel-primary post-editor"> 
          <div className="panel-heading"> 
            <h3 className="panel-title">Message list</h3> 
          </div> 
          {
            this.state.posts.map((postBody, idx) => {
              return (<Post key={idx} postBody={postBody} />)
            })
          }
        </div> 
		
		
			
          <div className="panel panel-primary post-editor"> 
              <div className="panel-heading"> 
                <h3 className="panel-title">Panel title</h3> 
              </div> 
            <PostEditor addPost={this.addPost} />  
           </div>
      </div>
    );
  }
}

export default App;
